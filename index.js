var app = angular.module('app', ['ngRoute']);
app.config(function ($routeProvider) {
    // configure the routes
    $routeProvider
        .when('/', {
            templateUrl: './components/inicio/inicio.html',
            controller: 'inicioCtrl'
        })
        .when('/student', {
            templateUrl: './components/student/student.html',
            controller: 'studentCtrl'
        })
        .when('/period', {
            templateUrl: './components/period/period.html',
            controller: 'periodCtrl'
        })

});
app.controller('inicioCtrl', function ($scope) {
    $scope.mensaje = "Sistema de muestra de hora del programa momentum"
});

app.controller('periodCtrl', function ($scope,$http) {
    $scope.registros;
    $scope.registros2;
    $scope.error = false;
    $scope.mensajeError = "";
    $scope.resMonths = false;
    $scope.resDates = false;
    $scope.months = false;
    $scope.dates = false;
    $scope.month = 1;
    $scope.getMonth = function(){
        $scope.error = false;
        $scope.resMonths = false;
        $scope.resDates = false;
        $scope.months = true;
        $scope.dates = false;
        $scope.registros = "";
        $scope.registros2 = "";
    }
    $scope.getBetween = function(){
        $scope.error = false;
        $scope.resMonths = false;
        $scope.resDates = false;
        $scope.months = false;
        $scope.dates = true;
        $scope.registros = "";
        $scope.registros2 = "";
    }
    $scope.getBetweenDates = function(){
        $http.post("http://localhost:8080/academyProject/api/v1/period/",
        {
            "startDate": document.getElementById("startDate").value,
            "endDate": document.getElementById("endDate").value
        })
        .then(function (respuesta) {
            $scope.registros2 = respuesta.data;
            $scope.resDates = true;
        })
        .catch(function (error) {
            console.log(error.data);
            $scope.error = true;
            $scope.mensajeError = error.data.exceptionMessage;
        });
    }
    $scope.getRegistros = function(){
        if($scope.month >9){
            $http.get("http://localhost:8080/academyProject/api/v1/period/"+$scope.month)
            .then(function(res){
                $scope.registros = res.data
                $scope.resMonths = true;
            })
            .catch(function(res){
                $scope.error = true;
                $scope.mensajeError = error.data.exceptionMessage;
            })
        }else{
            $http.get("http://localhost:8080/academyProject/api/v1/period/0"+$scope.month)
            .then(function(res){
                $scope.registros = res.data
                $scope.resMonths = true;
            })
            .catch(function(res){
                $scope.error = true;
                $scope.mensajeError = error.data.exceptionMessage;
            })
        }
    }
});

app.controller('studentCtrl', function ($scope,$http) {
    $scope.error = false;
    $scope.horas = false;
    $scope.horasTotales ="";
    $scope.resBetween = false;
    $scope.resHoras = false;
    $scope.mensajeError = "";
    $scope.registros = "";
    $scope.student= {
        is: "",
        fecha: 01,
        startDate: "",
        endDate: ""
    }
    $scope.between = false;

    $scope.horasTotales = 0;
    

    $scope.totalHours = function(){
        $scope.resBetween = false;
        $scope.error = false;
        $scope.resHoras = false;
        $scope.horas = true;
        $scope.between = false;
        $scope.horasTotales ="";
        $scope.registros = "";
    }

    $scope.userBetween = function(){
        $scope.resBetween = false;
        $scope.error = false;
        $scope.resHoras = false;
        $scope.between = true;
        $scope.horas = false;
        $scope.horasTotales ="";
        $scope.registros = "";
    }

    $scope.buscarHoras = function(){
        if($scope.student.fecha>9){
            $http.get("http://localhost:8080/academyProject/api/v1/users/"+$scope.student.is +
            "/"+$scope.student.fecha)
            .then(function (respuesta) {
                $scope.horasTotales = respuesta.data;
                console.log($scope.horasTotales);
                $scope.resHoras = true;
            })
            .catch(function (error) {
                console.log(error.data);
                
                $scope.error = true;
                $scope.mensajeError = error.data.exceptionMessage;
            });
        }else{
            $http.get("http://localhost:8080/academyProject/api/v1/users/"+$scope.student.is +
            "/0"+$scope.student.fecha)
            .then(function (respuesta) {
                $scope.resHoras = true;
                $scope.horasTotales = respuesta.data;
                console.log($scope.horasTotales);

            })
            .catch(function (error) {
                console.log(error.data);
                $scope.error = true;
                $scope.mensajeError = error.data.exceptionMessage;
            });
        }
    }

    $scope.buscarRegistros = function(){
        $http.post("http://localhost:8080/academyProject/api/v1/users/",
            {
                "is": document.getElementById("is").value,
                "startDate": document.getElementById("fechaIn").value,
                "endDate": document.getElementById("fechaOut").value
            }
        )
        .then(function (respuesta) {
            $scope.registros = respuesta.data;
            $scope.resBetween = true;
        })
        .catch(function (error) {
            console.log(error.data);

            $scope.error = true;
                $scope.mensajeError = error.data.exceptionMessage;
        });
    }
});
